DISTDIR=dist

RELEASEVERSION=$(shell echo ${VERSION})
DEBUGVERSION=$(shell git rev-list -1 HEAD | cut -c 1-10 )
RELEASEVERSION?=$(DEBUGVERSION)
HASH=$(shell git rev-list -1 HEAD)
GOVERSION=$(shell go version)
DATE=$(shell date)

LDFLAGS=-s -w
LDGENERICFLAGS=-X 'codeberg.org/snaums/mvoCI/core.BuildTime=$(DATE)' -X 'codeberg.org/snaums/mvoCI/core.Compiler=$(GOVERSION)'  -X 'codeberg.org/snaums/mvoCI/core.GitHash=$(HASH)'
DEBUGLDVERSION=-X 'codeberg.org/snaums/mvoCI/core.Version=dev-$(DEBUGVERSION)'
RELEASELDVERSION=-X 'codeberg.org/snaums/mvoCI/core.Version=$(RELEASEVERSION)'

DEBUGLDFLAGS=$(LDFLAGS) $(DEBUGLDVERSION) $(LDGENERICFLAGS)
RELEASELDFLAGS=$(LDFLAGS) $(RELEASELDVERSION) $(LDGENERICFLAGS)

all:
	go run -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go

debug:
	go run -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go --debug

install:
	go run -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go --install

mvobuild:
	go build -gccgoflags "-pthread" -ldflags "$(DEBUGLDFLAGS)" main.go
	mv main mvo
	#-upx mvo

release:
	go build -gccgoflags "-pthread" -ldflags "$(RELEASELDFLAGS)" main.go
	mv main mvo
	-upx mvo

package:
	rm -rf $(DISTDIR)
	mkdir $(DISTDIR)
	mv mvo $(DISTDIR)
	cp -r views $(DISTDIR)
	cp -r static $(DISTDIR)

dist: mvobuild
	make package
	make distclean

distrelease: release
	make package
	make distclean

distclean:
	rm -rf web hook log Makefile main.go mvo.cfg static repo static views core build builds auth .gitignore go.mod go.sum mvoBuild.sh
	mv $(DISTDIR)/* .
	rmdir $(DISTDIR)

clean:
	-rm mvo
	-rm -rf dist

dep:
	echo "Not needed anymore, use golangs modules"

doc:
	godoc -http=localhost:6060

vet:
	go vet

ctags:
	ctags -R --language-force=go .
