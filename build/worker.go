// There are n build workers, which are started at the start of mvoCI.
// They execute BuildWorker. When a new build object is spawned into the
// build queue / channel, it is fetched (from git repo), the buildscript
// is placed in the folder and executed with bash. Then the directory
// is zipped and later removed.
//
// Two builds cannot be ongoing on the same repo at the same time.
package build

import (
    "os"
    "sync"
    "os/exec"
    "time"
    "strings"
    "strconv"
    "math/rand"
    "io/ioutil"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/hook"
    "gorm.io/gorm"
)

// structure for storing a reference to the database and the global config
type BuilderConfig struct {
    cfg *core.Config;       // reference to the global config
    db *gorm.DB;            // reference tot he global database connection
};

// global config used by the workers
var c BuilderConfig;

// thread identifiers to the worker threads
var Workers map[int]core.Thread;
// channel / build queue for communication with the build workers
var InChannel chan core.Build

// string builder wrapper
type mockerBuilder struct {
    strings.Builder
}

// append a string to the builder
func (m* mockerBuilder) Write(p []byte) (n int, err error) {
    m.append ( string(p) )
    return 0,nil
}

// append a string to the builder
func (m* mockerBuilder) append ( args... string) {
    for _, str := range args {
        m.WriteString ( str );
    }
}

// Mutex for synchronizing the workers.
// The workers cannot at the same time all get a repo from the channel, try to find out if its already being build and then start building it.
// That would lead to the same repo being build at the same time, which is handled rather unwell at the moment.
var mux sync.Mutex;

// sets up the worker threads in the given quantitiy
func SetupWorkerThreads ( cfg *core.Config, db* gorm.DB ) {
    c.cfg = cfg
    c.db = db
    numThreads := cfg.ParallelBuilds

    // cleanup build directory
    os.RemoveAll ( c.cfg.Directory.Repo )

    InChannel = make( chan core.Build, 50 )
    Workers = make(map[int]core.Thread)
    for i:=0; i < numThreads; i++ {
        Workers[i] = core.Pthread_create ( BuildWorker )
    }

    // builds, which where started or enqueued before the server was shut down -> enqueue them again 
    // and try to (re)build
    var b []core.Build
    c.db.Model ( &core.Build{} ).Preload("Repository").Where ("status = ? OR status = ?", "enqueued", "started").Find ( &b );
    for _, bs := range b {
        // enqueue
        //c.db.Model (&bs).Association("Repository").Find(&bs.Repository);
        bs.Status = "enqueued";
        c.db.Save ( &bs )
        InChannel <- bs
    }
}

// worker function: loop indefinitely and retrieve a new build from the build
// queue and build it.
func BuildWorker () {
    core.Console.Log ( "Starting BuildWorker" )
    var b core.Build
    var cnt int64
    for true {
        b = core.Build{}
        b =<-InChannel
        mux.Lock();
        c.db.Model ( &core.Build{} ).Where ( "repository_id = ? AND status = 'started'", b.RepositoryID ).Count ( &cnt );
        // if there is another thread building the same repo, reject it and back off
        if cnt > 0 {
            core.Console.Log ("Rejecting Build, another one on the same repo is running");
            InChannel <- b
            mux.Unlock();
            time.Sleep ( 5 * time.Second );
        } else {
            b.Status = "started";
            b.StartedAt = time.Now()
            c.db.Save ( &b );
            mux.Unlock();
            core.Console.Log("Starting Build on ", b.Repository.Name )
            var log mockerBuilder

            var dir string
            var try int = 5
            var err bool = false
            for dir == "" && err == false {
                dir = c.cfg.Directory.Repo + dirNameFromRepo ( b.Repository ) + "-" + strconv.Itoa(rand.Intn(100000))
                if st, errstat := os.Stat(dir); errstat == nil && st.IsDir() {
                    dir = ""
                    try--;
                    if try <= 0 {
                        log.append("> Cannot find a suitable directory")
                        err = true
                        break;
                    }
                }
            }

            if err == false{
                _, err = WorkerGoGet ( &b, dir, &log );
            }

            b.FinishedAt = time.Now()
            if err != true {
                log.append ( "> Build failed\n");
                b.Log = log.String();
                b.Status = "failed";

                core.Console.Log("Finished Build on ", b.Repository.Name )
                c.db.Save ( &b );

                WorkerGoCleanup ( &b, &log, dir )
            } else {
                log.append ( "> Build successful\n");
                b.Log = log.String();
                b.Status = "finished"

                c.db.Save ( &b );
            }
        }
        //core.LogLn ( b.Log );
    }
}

// starts building  a build, i.e. clone the repository, get the correct branch
// and commit, and calling every line of the build-script (WorkerGoBuild)
// if this all succeeds: set the status to "finished", "failed" otherwiese.
func WorkerGoGet ( b *core.Build, dir string, log *mockerBuilder ) (*mockerBuilder, bool) {
    // create repo directory
    log.append( "> Started build of ", b.Repository.Name, "\n" )

    err := os.Mkdir ( c.cfg.Directory.Repo, 0777 );
    err = os.Mkdir ( c.cfg.Directory.Build, 0777 );

    log.append ( "> Cloning repository\n" )

    out, err := exec.Command ("git", "clone", b.Repository.CloneUrl, dir ).CombinedOutput()
    log.append( string(out) )
    if err != nil {
        log.append("> Could not clone the repository :/\nLog: ", err.Error())
        return log, false;
    }

    log.append ( "> Switching branch to ", b.Branch, "\n");
    cmd := exec.Command ("git", "checkout", b.Branch )
    cmd.Dir = dir;
    out, err = cmd.CombinedOutput()
    log.append ( string(out) )
    if err != nil {
        log.append("> Could not switch to branch: ", err.Error(), "\n")
        return log, false;
    }

    cmd = exec.Command ("git", "rev-parse", "HEAD" )
    cmd.Dir = dir
    out, err = cmd.CombinedOutput()
    if err != nil {
        log.append("> Could not find out the current commit SHA: ", err.Error(), "\n")
        return log, false;
    }

    log.append ("> HEAD is ", string(out), "\n" )
    if len(b.CommitSha) > 0 && b.CommitSha != string(out) {
        log.append ( "> Fetching Commit ", b.CommitSha, "\n");

        cmd = exec.Command ("git", "checkout", b.CommitSha )
        cmd.Dir = dir
        out, err = cmd.CombinedOutput()
        log.append ( string(out) )
        if err != nil {
            log.append("> Could fetch the appropriate commit :/", err.Error(), "\n")
            return log, false;
        }
    } else {
        b.CommitSha = string(out)
        log.append ("> Building HEAD\n" )
    }

    // read author and commit-message from the git repository
    if b.CommitAuthor == "" || b.CommitMessage == "" {
        cmd = exec.Command ( "git", "show", "--format=%cn|%s", "--no-abbrev-commit", "--no-notes", "--no-patch" );
        cmd.Dir = dir;
        out, err = cmd.CombinedOutput();
        o := strings.Split ( string(out), "\n" );
        o2 := strings.Split ( o[0], "|");
        b.CommitAuthor = strings.TrimSpace ( o2[0] );
        b.CommitMessage = strings.TrimSpace ( o2[1] );
    }

    return WorkerGoBuild ( b, log, dir )
}

// Build the repo by executing every line of the build script
func WorkerGoBuild ( b *core.Build, log *mockerBuilder, dir string ) (*mockerBuilder, bool) {
    log.append ( "> Starting Build ... \n")

    var ShellScript string
    ShellScript = b.BuildScript.ShellScript
    buildScript := []byte( "set -e\n" + strings.ReplaceAll ( ShellScript, "\r", "\n") )
    buildScriptName := dir + "/mvoBuild.sh"
    err := ioutil.WriteFile ( buildScriptName, buildScript, 0777 )
    if err != nil {
        log.append ( "> Could not create build script\n")
        return log, false
    }

    l, e := execBuildScript ( "mvoBuild.sh", dir, b )
    log.append ( l )
    if e != true {
        return log, false
    }

    return WorkerGoZip ( b, log, dir );
}

// Zips the repo directory with the builds
func WorkerGoZip ( b *core.Build, log *mockerBuilder, dir string ) (*mockerBuilder, bool) {
    if b.Repository.KeepBuilds {
        var ZipName string = dirNameFromRepo ( b.Repository ) + "_" +
                             strconv.Itoa(int(b.ID)) + ".tar." +
                             c.cfg.CompressionMethod;

        ZipName = strings.ReplaceAll ( ZipName, " ", "_" )
        var ZipFile string = "../../" + c.cfg.Directory.Build + ZipName

        os.RemoveAll ( dir + "/.git" )
        log.append ( "> Zipping directory ", dir, " to ", ZipFile, "\n" )
        cmd := exec.Command ( "tar", "--auto-compress", "--create", "-v", "--file="+ZipFile, "." )
        cmd.Dir = dir
        out, err := cmd.CombinedOutput ()
        log.append( string(out) )
        if err != nil {
            log.append ( "Zipping failed: ", err.Error(), "\n" )
            return log, false
        }

        b.Zip = ZipName
        if b.Api == "gitea" && b.Event == "release" {
            // only gitea has release-api event implemented, atm. (2020-06-27)
            return WorkerPushRelease ( b, log, dir );
        }
    } else {
        b.Zip = ""
        log.append ("> Skipping zipping because of repo-local configuration");
    }
    return WorkerGoCleanup ( b, log, dir )
}

// atm for gitea we can push release builds to the code server using OAuth2 authentication (set-up beforehand by the user)
func WorkerPushRelease ( b *core.Build, log *mockerBuilder, dir string ) (*mockerBuilder, bool ) {
    switch b.Api {
        case "gitea":
            err := hook.GiteaPushRelease ( b, b.ApiUrl, c.cfg.Directory.Build + "/" + b.Zip, c.cfg, c.db );
            if err != nil {
                log.append ("Error pushing the artifact: ", err.Error() );
            } else {
                log.append ("Pushing artifact should have worked.");
            }
    }
    return WorkerGoCleanup ( b, log, dir );
}

// removes the repo directory
func WorkerGoCleanup ( b *core.Build, log *mockerBuilder, dir string ) (*mockerBuilder, bool) {
    log.append ( "> Cleaning up build directory\n" )
    os.RemoveAll ( dir );
    return log, true;
}

// execute the shell build script
func execBuildScript ( file string, dir string, b *core.Build ) (string, bool) {
    var l mockerBuilder;
    l.append ( "> Executing build script '", file, "'\n" )
    cmd := exec.Command ( "bash", file )
    cmd.Env = os.Environ();
    cmd.Env = append ( cmd.Env, "VERSION=" + b.CommitSha );
    cmd.Env = append ( cmd.Env, "MVO_REPO=" + b.Repository.Name );
    cmd.Env = append ( cmd.Env, "MVO_BRANCH=" + b.Branch );
    cmd.Env = append ( cmd.Env, "MVO_COMMIT_SHA=" + b.CommitSha );
    cmd.Env = append ( cmd.Env, "MVO_COMMIT_AUTHOR=" + b.CommitAuthor );
    cmd.Env = append ( cmd.Env, "MVO_COMMIT_MESSAGE=" + b.CommitMessage );
    cmd.Dir = dir;
    out, err := cmd.CombinedOutput();
    l.append ( string(out) )
    if err != nil {
        l.append ( "> Execution failed with error: ", err.Error(), "\n" )
        return l.String(), false;
    }

    return l.String(), true;
}

// returns the name of the repository for usage as directory name
func dirNameFromRepo ( r core.Repository ) string {
    return r.Name;
}
