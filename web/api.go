// handlers for /api

package web

import (
    "strconv"
    //"regexp"
    "errors"
    "net/http"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/build"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

// type for a handler-function for API-calls.
type apiHandlerFn func ( endpoint string, user core.User, ctx echo.Context ) error;

// type for error-definitions in the routing table
type apiActionErrorT struct {
    Status int                      // the returned status
    Reason string                   // the meaning, if that status was discovered liv
}
// actions represent fine grain handlers, e.g. edit, list,...
type apiActionT struct {
    Fn apiHandlerFn                 // pointer to a handler function
    Parameters []apiParameterT      // listing of allowed parameters
    Brief string                    // a short statement about what it is supposed to do
    Note string                     // a note, maybe a warning of something may go wrong
    Returns string                  // a statement about what it is supposed to return
    Method string                   // the allowed method (POST, GET, PUT, ...)
    Errors []apiActionErrorT        // a listing of errors, the call may return
}
// The handler type represents a broader range of handler, like a heading in a text.
// May or may not be callable itself
type apiHandlerT struct {
    Descriptor apiActionT           // descriptor and head-level action
    Actions map[string]apiActionT   // a list of sub-actions
}
// representation of parameters to API-endpoints
type apiParameterT struct {
    Name string                     // name of the parameter
    Ptype string                    // type of the parameter
    Brief string                    // a short summary of the meaning of the parameter
    Optional bool                   // can the parameter be omitted?
}

// internal routining table for API-Calls
var apiRoutingTable = map[string]apiHandlerT {
    "repo" : apiHandlerT {
        Descriptor: apiActionT {
            Brief: "Query and edit repository information",
        },
        Actions: map[string]apiActionT {
            "detail": apiActionT {
                Fn: apiRepoDetailHandler,
                Method: "GET",
                Brief: "Query details about a given repository",
                Note: "only possible on own repositories, if not superuser",
                Parameters: []apiParameterT{
                    apiParameterT { Name: "id", Ptype: "int", Brief: "The id of the effected repository", Optional: false },
                },
                Returns: "JSON Object representation of the repository, note: the user-field is empty",
                Errors: []apiActionErrorT {
                    apiActionErrorT { Status: http.StatusNotFound, Reason: "Invalid action given" },
                    apiActionErrorT { Status: http.StatusBadRequest, Reason: "id invalid or not an integer" },
                },
            },
            "list": apiActionT {
                Fn: apiRepoListHandler,
                Method: "GET",
                Brief: "List all queryable repositories, if name is given it is used as search-word",
                Note: "will only list own and public repositories, if you are not superuser",
                Parameters: []apiParameterT{
                    apiParameterT { Name: "name", Ptype: "string", Brief: "The pattern to be used for searching", Optional: true },
                    apiParameterT { Name: "order_by", Ptype: "string", Brief: "Field for ordering (valid are: created_at, updated_at, name, public)", Optional: true },
                    apiParameterT { Name: "length", Ptype: "uint", Brief: "Number of returned objects" , Optional: true},
                    apiParameterT { Name: "start_at", Ptype: "uint", Brief: "The number to start at (starting at 0)", Optional: true },
                },
                Returns: "JSON Array of all queryable repositories, note: only ID, Name, UserID and CloneURL are set",
                Errors: []apiActionErrorT {},
            },
            "build": apiActionT {
                Fn: apiRepoBuildHandler,
                Method: "GET",
                Brief: "Build the given repository",
                Note: "will only work, when you are the owner or superuser",
                Parameters: []apiParameterT{
                    apiParameterT { Name: "id", Ptype: "int", Brief: "ID of the effected repository", Optional: false },
                    apiParameterT { Name: "branch", Ptype: "string", Brief: "The branch to be build", Optional: true },
                    apiParameterT { Name: "hash", Ptype: "string", Brief: "The commit to be build", Optional: true },
                },
                Returns: "JSON Representation of the created Build-object, returns immediately",
                Errors: []apiActionErrorT {
                    apiActionErrorT { Status: http.StatusNotFound, Reason: "Invalid action given" },
                    apiActionErrorT { Status: http.StatusBadRequest, Reason: "id invalid or not an integer" },
                },
            },
        },
    },
    "build": {
        Descriptor: apiActionT {
            Brief: "Query information about a build",
            Fn: apiBuildInfoHandler,
            Method: "GET",
            Note: "omits the Log-attribute",
            Parameters: []apiParameterT {
                apiParameterT { Name: "id", Ptype: "uint", Brief: "ID of the effected Build" },
            },
            Returns: "Current values of the Build object",
            Errors: []apiActionErrorT {
                apiActionErrorT { Status: http.StatusForbidden, Reason: "You do not have the permission to query the build" },
                apiActionErrorT { Status: http.StatusNotFound, Reason: "There is no Build with the given ID" },
                apiActionErrorT { Status: http.StatusBadRequest, Reason: "id invalid or not an integer" },
            },
        },
        Actions: map[string]apiActionT {
            "log": apiActionT {
                Fn: apiBuildLogHandler,
                Method: "GET",
                Brief: "Get the build log for a given build",
                Note: "",
                Parameters: []apiParameterT{
                    apiParameterT { Name: "id", Ptype: "int", Brief: "ID of the effected repository", Optional: false },
                },
                Returns: "JSON Object { log: [log] }",
                Errors: []apiActionErrorT {
                    apiActionErrorT { Status: http.StatusForbidden, Reason: "You do not have the permission to query the build" },
                    apiActionErrorT { Status: http.StatusNotFound, Reason: "There is no Build with the given ID" },
                    apiActionErrorT { Status: http.StatusBadRequest, Reason: "id invalid or not an integer" },
                },
            },
            "list": apiActionT {
                Fn: apiBuildListHandler,
                Method: "GET",
                Brief: "Get a list of builds of a repository (matching a pattern if name is given)",
                Note: "Searches in the hash, commit-message and commit-author-field of the builds",
                Parameters: []apiParameterT{
                    apiParameterT { Name: "id", Ptype: "string", Brief: "The pattern to be used for searching", Optional: false },
                    apiParameterT { Name: "name", Ptype: "string", Brief: "The pattern to be used for searching", Optional: true },
                    apiParameterT { Name: "order_by", Ptype: "string", Brief: "Field for ordering (valid are: CreatedAt, StartedAt, FinishedAt, Status, CommitSha, Event, Branch)", Optional: true },
                    apiParameterT { Name: "length", Ptype: "uint", Brief: "Number of returned objects", Optional: true},
                    apiParameterT { Name: "start_at", Ptype: "uint", Brief: "The number to start at (starting at 0)", Optional: true },
                },
                Returns: "JSON Array of -objects or empty JSON-Array if none are found",
                Errors: []apiActionErrorT {
                    apiActionErrorT { Status: http.StatusBadRequest, Reason: "id parameter missing" },
                    apiActionErrorT { Status: http.StatusNotFound, Reason: "The repository was not found" },
                },
            },
        },
    },
    "user": {
        Descriptor: apiActionT {
            Brief: "Query information about a user",
            Fn: apiUserInfoHandler,
            Method: "GET",
            Note: "omits the password-attribute",
            Parameters: []apiParameterT {
                apiParameterT { Name: "id", Ptype: "uint", Brief: "ID of the effected User" },
            },
            Returns: "Current values of the User object",
            Errors: []apiActionErrorT {
                apiActionErrorT { Status: http.StatusNotFound, Reason: "There is no user with the given ID" },
                apiActionErrorT { Status: http.StatusBadRequest, Reason: "id invalid or not an integer" },
            },
        },
    },
}

// lists the allowed API-Calls on the website, if enabled in the config
func apiListingHandler ( ctx echo.Context ) error {
    if s.cfg.Api.Enable == false || s.cfg.Api.ListingEnable == false {
        return HTTPError ( http.StatusNotImplemented, ctx );
    }

    user := core.User{};
    var methods []string = []string{"GET"}
    if UserFromSession ( &user, ctx ) != true {
        if s.cfg.Api.ListingNeedsAuth {
            return HTTPError ( http.StatusNotImplemented, ctx );
        }
        return ctx.Render ( http.StatusOK, "api_listing", echo.Map{
            "navPage": "api",
            "routingTable": apiRoutingTable,
            "methods": methods,
            "user": nil,
            "apiHost": ctx.Scheme() + "://" + ctx.Request().Host,
        });
    } else {
        return ctx.Render ( http.StatusOK, "api_listing", echo.Map{
            "navPage": "api",
            "routingTable": apiRoutingTable,
            "methods": methods,
            "user": user,
            "apiHost": ctx.Scheme() + "://" + ctx.Request().Host,
        });
    }
}

// generic API-handler.
// All API-calls are firstly handled by this function, which then
// routes the query to the correct function
func apiHandler ( ctx echo.Context ) error {
    if s.cfg.Api.Enable == false {
        return HTTPError ( http.StatusNotImplemented, ctx );
    }
    user := core.User{};
    if ApiTokenFromSession ( &user, ctx ) != true {
        // TODO replace with api-error?
        return HTTPError ( http.StatusForbidden, ctx );
    }

    endpoint := ctx.Param ( "endpoint" );
    action   := apiGetAction ( ctx );
    method   := ctx.Request().Method;

    var d apiActionT;
    if action == "" {
        d = apiRoutingTable[endpoint].Descriptor;
    } else {
        d = apiRoutingTable[endpoint].Actions[action];
    }
    if d.Fn == nil || d.Method != method {
        return HTTPError ( http.StatusBadRequest, ctx );
    }

    return d.Fn ( endpoint, user, ctx );
}


// helper: retrieves a string parameter (by name) from the context, either from param (GET) or FormValue (mostly used by POST)
func apiGetParam ( ctx echo.Context, param string ) ( string, error ) {
    value := ctx.Param ( param );
    if value == "" {
        value = ctx.FormValue ( param );
    }

    if value == "" {
        return "", errors.New ("Value missing");
    }
    return value, nil;
}

// helper: retrieves a number (by name) from the context, either from param (GET) or FormValue (mostly used by POST)
func apiGetNumber ( ctx echo.Context, param string ) ( int, error ) {
    i, err := strconv.Atoi ( ctx.Param ( param ) )
    if err != nil {
        i, err = strconv.Atoi ( ctx.FormValue ( param ) );
        if err != nil {
            return -1, err;
        }
    }
    return i,nil;
}

// helper: shorthand for action retrieval
func apiGetAction ( ctx echo.Context ) ( string ) {
    str, _ := apiGetParam ( ctx, "action" )
    return str;
}

// shorthand for id retrieval
func apiGetID ( ctx echo.Context ) ( int, error ) {
    return apiGetNumber ( ctx, "id" )
}


func apiUserHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    return HTTPError ( http.StatusForbidden, ctx );
}

// implements a in b - query. I.e. is an element a in a list b
func in ( a string, b []string ) bool {
    for _, v := range b {
        if a == v {
            return true
        }
    }

    return false;
}

// API: handler call for the search function over builds
func apiBuildListHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    var allowedOrders []string = []string{"created_at", "started_at", "finished_at", "status", "commit_sha", "commit_author", "event", "branch" };
    id, err := apiGetID ( ctx );
    if err != nil {
        return HTTPError ( http.StatusBadRequest, ctx );
    }

    name, err := apiGetParam ( ctx, "name" );
    if err != nil {
        name = ""
    } else {
        name = "%" + name + "%";
    }

    orderby, err := apiGetParam ( ctx, "order_by" );
    if err != nil {
        orderby = allowedOrders[0] + " DESC";
    }

    if !in(orderby, allowedOrders) {
        orderby = allowedOrders[0] + " DESC";
    }

    limit, err := apiGetNumber ( ctx, "limit");
    if err != nil {
        limit = 10;
    }

    startAt, err := apiGetNumber ( ctx, "start_at" );
    if err != nil {
        startAt = 0;
    }

    var r core.Repository;
    if user.Superuser {
        s.db.Model ( &core.Repository{} ).Select("id").Where("id = ?", id ).First ( &r );
    } else {
        s.db.Model ( &core.Repository{} ).Select("id").Where("id = ? AND (public = ? OR user_id = ?)", id, true, user.ID ).First ( &r );
    }

    if r.ID <= 0 {
        return HTTPError( http.StatusNotFound, ctx );
    }

    var b []core.Build;
    if name == "" {
        s.db.Select("id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event").Order( orderby ).Offset( startAt ).Limit(limit).Find ( &b );
    } else {
        s.db.Select("id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event").Where ("commit_message LIKE ? OR commit_sha LIKE ? OR commit_author LIKE ?", name, name, name ).Order( orderby ).Offset( startAt ).Limit(limit).Find ( &b );
    }
    ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
    return ctx.JSON ( http.StatusOK, b );
}

// API: return the log of a specified build
func apiBuildLogHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    id, err := apiGetID (ctx);
    if err != nil || id <= 0 {
        return HTTPError ( http.StatusBadRequest, ctx );
    }

    var b core.Build;
    var r core.Repository;
    s.db.Preload("Repository").Select("id, log, repository_id").Where ("id = ?", id).First( &b );
    if b.ID <= 0 {
        return HTTPError ( http.StatusNotFound, ctx );
    }

    r = b.Repository
    if r.ID <= 0 {
        return HTTPError ( http.StatusNotFound, ctx );
    }

    if user.Superuser || user.ID == r.UserID {
        ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
        return ctx.JSON ( http.StatusOK, struct{Log string} { b.Log }  );
    }
    return HTTPError ( http.StatusForbidden, ctx );
}

// API: return information about the build like, in a sense everything except the log
func apiBuildInfoHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    id, err := apiGetID (ctx);
    if err != nil || id <= 0 {
        return HTTPError ( http.StatusBadRequest, ctx );
    }

    var b core.Build;
    var r core.Repository;
    s.db.Preload("Repository").Select("id, created_at, updated_at, finished_at, started_at, status, commit_sha, branch, zip, repository_id, commit_author, commit_message, commit_url, event").Where ("id = ?", id).First( &b );
    if b.ID <= 0 {
        return HTTPError ( http.StatusNotFound, ctx );
    }

    r = b.Repository
    if r.ID <= 0 {
        return HTTPError ( http.StatusNotFound, ctx );
    }

    if user.Superuser || user.ID == r.UserID {
        ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
        return ctx.JSON ( http.StatusOK, b );
    }
    return HTTPError ( http.StatusForbidden, ctx );
}

// API: api call handler for listing and searching over repositories
func apiRepoListHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    var allowedOrders []string = []string{"name", "created_at", "updated_at", "public" };
    name, err := apiGetParam ( ctx, "name" );
    if err != nil {
        name = "";
    } else {
        name = "%" + name + "%";
    }

    orderby, err := apiGetParam ( ctx, "order_by" );
    if err != nil {
        orderby = allowedOrders[0]
    }

    if !in(orderby, allowedOrders) {
        orderby = allowedOrders[0];
    }

    limit, err := apiGetNumber ( ctx, "limit");
    if err != nil {
        limit = 10;
    }

    startAt, err := apiGetNumber ( ctx, "start_at" );
    if err != nil {
        startAt = 0;
    }

    var r []core.Repository;
    if user.Superuser {
        if name == "" {
            s.db.Select("id, name, user_id, clone_url, public").Order( orderby ).Offset( startAt ).Limit(limit).Find ( &r );
        } else {
            s.db.Select("id, name, user_id, clone_url, public").Where ("name LIKE ?", name ).Order( orderby ).Offset( startAt ).Limit(limit).Find ( &r );
        }
    } else {
        if name == "" {
            s.db.Select("id, name, user_id, clone_url, public").Where ("user_id = ?", user.ID ).Order( orderby ).Offset( startAt ).Limit(limit).Find ( &r );
        } else {
            s.db.Select("id, name, user_id, clone_url, public").Where ("name LIKE ? AND user_id = ?", name, user.ID ).Order( orderby ).Offset( startAt ).Limit(limit).Find ( &r );
        }
    }

    ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
    return ctx.JSON ( http.StatusOK, r )
}

// API: return information about a given user
func apiUserInfoHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    id, err := apiGetID (ctx);
    if err != nil || id <= 0 {
        return HTTPError ( http.StatusBadRequest, ctx );
    }

    var u core.User;
    s.db.Select("id, created_at, name, email, superuser").Where ("id = ?", id).First( &u );
    if u.ID <= 0 {
        return HTTPError ( http.StatusNotFound, ctx );
    }
    ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
    return ctx.JSON ( http.StatusOK, u );
}

// API: return a repository object by its ID
func apiRepoDetailHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    id, err := apiGetID (ctx);
    if err != nil || id <= 0 {
        return HTTPError ( http.StatusBadRequest, ctx );
    }

    var r core.Repository;
    s.db.Where ("id = ?", id).First( &r );
    if r.ID <= 0 {
        return HTTPError ( http.StatusNotFound, ctx );
    }
    if user.Superuser || user.ID == r.UserID {
        ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
        return ctx.JSON ( http.StatusOK, r );
    }
    return HTTPError ( http.StatusForbidden, ctx );
}

// API: Start a build using the API with a given branch and hash
func apiRepoBuildHandler ( endpoint string, user core.User, ctx echo.Context ) error {
    id, err := apiGetID (ctx);
    if err != nil || id <= 0 {
        return HTTPError ( http.StatusBadRequest, ctx );
    }
    var r core.Repository
    s.db.Where ("id = ?", id).First ( &r )
    if r.ID > 0 {
        if r.UserID == user.ID || user.Superuser {
            var hash = ctx.FormValue ( "hash" );
            var branch = ctx.FormValue ("branch");
            if branch == "" {
                branch = r.DefaultBranch;
            }

            b := build.BuildSpecific2 ( s.db, r, branch, hash, "", "", "", "API Build" )
            ctx.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJSONCharsetUTF8)
            var bx core.Build;
            s.db.Where ("id = ?", b).First ( &bx );
            return ctx.JSON ( http.StatusOK, bx )
        } else {
            return HTTPError ( http.StatusForbidden, ctx );
        }
    }
    return HTTPError(http.StatusNotFound, ctx);
}

