// Handlers for /build functions

package web

import (
    "os"
    "time"
    //"fmt"
    "strconv"
    //"regexp"
    "net/http"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/build"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

// calculate the duration of a build from finish and start-time
func CalcBuildDuration ( b []core.Build ) {
    for i,_ := range b {
        bi := &b[i]
        if bi.Status == "finished" || bi.Status == "failed" {
            d := bi.FinishedAt.Sub (bi.StartedAt)
            bi.Duration = d.Truncate(time.Millisecond).String ()
        } else {
            bi.Duration = "-"
        }
    }
}

// /zip/:file
// find and download a zip file, deny if the user has no permissions
func zipDownloadHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    file := ctx.Param ( "file" );
    var b core.Build;
    var r core.Repository
    s.db.Select("zip, id, repository_id").Where("zip = ?", file).First( &b );
    s.db.Select("id", "user_id").Where("id = ?", b.RepositoryID).First ( &r );
    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclView ) {
        return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(int(b.RepositoryID)) )
    }

    return ctx.File ( s.cfg.Directory.Build + file );
}

// /build/:id
// render the build view
func buildViewHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var b core.Build;
    var r core.Repository;
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    s.db.Where("id = ?", id).First ( &b )
    s.db.Select ("name, id, user_id").Where("id = ?", b.RepositoryID).First( &r )

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclView ) {
        return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(int(b.RepositoryID))+"?error=Permission Error" )
    }

    unfinished := false
    b.Duration = ""
    if b.Status == "finished" || b.Status == "failed" {
        d := b.FinishedAt.Sub (b.StartedAt)
        b.Duration = d.Truncate(time.Millisecond).String ()
    } else {
        unfinished = true
    }

    return ctx.Render ( http.StatusOK, "build", echo.Map{
        "navPage": "repo",
        "user": user,
        "repo": r,
        "acl": acl,
        "build": b,
        "unfinished": unfinished,
    })
}

// /build/log/:id
// return the build log as string
func buildLogHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var b core.Build;
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    var r core.Repository
    s.db.Select("log, repository_id").Where("id = ?", id).First ( &b )
    s.db.Select("user_id, id").Where("id = ?", b.RepositoryID).First ( &r )

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclView ) {
        return ctx.String ( 403, "Permission denied" )
    }

    if len(b.Log) != 0 {
        return ctx.String ( http.StatusOK, b.Log )
    }

    return HTTPError ( 404, ctx );
}

// /build/delete/:id
// delete the build and its artifact
func buildDeleteHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    var b core.Build
    var r core.Repository
    s.db.Select("id, repository_id, zip").Where("id = ?", id).First ( &b )
    s.db.Select("user_id, id").Where("id = ?", b.RepositoryID).First ( &r )

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclDelete ) {
        return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error" )
    }

    r.ID = buildDelete ( uint(id) )
    return ctx.Redirect ( http.StatusFound, "/repo/view/" + strconv.Itoa(int(r.ID)))
}

// helper function to delete a build from the database and the artifact from the file system
func buildDelete ( id uint ) uint {
    var rID uint = 0
    var b core.Build;

    s.db.Select("id, repository_id, zip").Where("id = ?", id).First ( &b )
    rID = b.RepositoryID
    os.Remove ( s.cfg.Directory.Build + b.Zip )
    s.db.Delete( &b )
    return rID
}

// /build/rebuild/:id
// rebuild a given build with the current settings
func buildRebuildHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    var b core.Build;
    var id int;
    id, err := strconv.Atoi(ctx.Param("id"))
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/repo?error=Repo not found" )
    }
    var bID uint = 0
    var r core.Repository = core.Repository{}
    s.db.Where("id = ?", id).First ( &b )
    s.db.Select("id, user_id").Where("id = ?", b.RepositoryID).First ( &r )
    if r.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/repo?error=Repo not found" )
    }

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclExecute ) {
        return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(int(r.ID))+"?error=Permission Error")
    }

    bID = build.ReBuild ( b, s.db );
    return ctx.Redirect ( http.StatusFound, "/build/" + strconv.Itoa(int(bID)) )
}
