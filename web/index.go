// dashboard, index, login

package web

import (
    "errors"
    "net/http"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/auth"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

// /dashboard
// renders the dashboard with all current builds listed
func dashboardHandler ( ctx echo.Context, user core.User ) error {
    var b []core.Build;
    var bq []core.Build
    var repo_ids []uint

    mine := ctx.QueryParam("mine")
    queryRecent := s.db.Select("builds.id", "finished_at", "started_at", "status", "commit_sha", "branch", "repository_id", "commit_message", "zip").Joins("Repository")
    queryQueue := s.db.Select("builds.id", "finished_at", "started_at", "status", "commit_sha", "branch", "repository_id", "commit_message", "zip").Joins("Repository")
    if mine != "" {
        // recent builds
        queryRecent.Where("repository.user_id = ?", user.ID)
        // build queue
        queryQueue.Where("(status='started' OR status='enqueued') AND repository.user_id = ?", user.ID)
    } else {
        if user.Superuser {
            // no where clause needed on recent builds
            // build queue
            queryQueue.Where("(status='started' OR status='enqueued')")
        } else {
            repo_ids = accessibleRepoIds( user )
            // recent builds
            queryRecent.Where("repository_id IN (?) OR repository.user_id = ?", repo_ids, user.ID)
            // build queue
            queryQueue.Where("(status='started' OR status='enqueued') AND (repository_id IN (?) OR repository.user_id = ?)", repo_ids, user.ID)
        }
    }

    queryRecent.Order("builds.created_at desc").Limit(10).Find ( &b )
    queryQueue.Order("status desc, builds.created_at asc").Limit(10).Find ( &bq )

    CalcBuildDuration ( b );
    CalcBuildDuration ( bq );

    return ctx.Render ( http.StatusOK, "dashboard", echo.Map{
        "builds" : b,
        "navPage": "dashboard",
        "user": user,
        "queue": bq,
    })
}

// redirects either to the dashboard, login-form or installation pages if enabled
// /
func indexHandler ( ctx echo.Context ) error {
    if s.cfg.Install == true {
        return ctx.Redirect(http.StatusFound, "/install/1")
    }
    u := core.User{};
    if UserFromSession ( &u, ctx ) == true {
       return dashboardHandler ( ctx, u );
    }
    return ctx.Redirect(http.StatusFound, "/login")
}

// destroys the loginToken of the current users, i.e. logging her out
// /logout
func doLogoutHandler ( ctx echo.Context ) error {
    RemoveLoginToken ( ctx )
    return ctx.Redirect(http.StatusFound, "/login")
}

// check the Credentials of a user and create a LoginToken for her if they are
// correct.
func doLoginHandler ( ctx echo.Context ) error {
    var cnt int64
    var err error
    var u core.User;
    var l core.LoginToken
    var prov core.AuthProvider
    var pr core.AuthProvider

    step, stepExtra := loginStepFromToken ( ctx );

    extra := ctx.FormValue ( "auth_extra" );
    given := ctx.FormValue ( "auth_secret" );
    if step == "fin" {
        // this should not happen
        return ctx.Redirect (http.StatusFound, "/login" );
    } else if step == "fail" {
        RemoveLoginToken ( ctx );
        return ctx.Redirect ( http.StatusFound, "/login" );
    } else if step == "" {
        // Verify Main
        s.db.Model(&core.User{}).Where("name = ?", extra).Count( &cnt ).First( &u );
        // TODO native should be some sort of default
        s.db.Where( "user_id = ? AND type = ?", u.ID, "native" ).Select("extra").First ( &pr )
        err = auth.VerifyMain ( s.db, u, given, extra, pr.Extra )
        NextStep := auth.FollowUp ( u, step );
        s.db.Where( "user_id = ? AND type = ?", u.ID, NextStep ).Select("extra").First ( &prov )
        NextExtra := auth.SeedStep ( NextStep, prov.Extra );

        if err == nil {
            // we check the count of users with this name here to mitigate timing attacks
            if cnt == 1 {
                if NewLoginToken ( &l, ctx, u, NextStep, NextExtra ) {
                    ctx.Redirect ( http.StatusFound, "/login" )
                }
            }
            err = errors.New ("Invalid Auth Operation")
        }

        return ctx.Redirect ( http.StatusFound, "/login?error="+err.Error() )

    } else {
        if UserFromIncompleteSession ( &u, ctx ) == false {
            u = core.User{}
        }
        s.db.Where( "user_id = ? AND type = ?", u.ID, step ).Select("extra").First ( &pr )
        core.Console.Warn ( "VerifyExtra... ",step, u,pr )
        err = auth.VerifyExtra ( s.db, u, step, stepExtra, given, extra, pr.Extra )

        NextStep := auth.FollowUp ( u, step );
        s.db.Where( "user_id = ? AND type = ?", u.ID, NextStep ).Select("extra").First ( &prov )
        NextExtra := auth.SeedStep ( NextStep, prov.Extra );

        if err == nil {
            if u.Name != "" {
                if ChangeLoginToken ( ctx, u, NextStep, NextExtra ) == true {
                    ctx.Redirect ( http.StatusFound, "/login" )
                }
            }
            err = errors.New ("Invalid Auth Operation")
        }

        if u.Name != "" {
            s.db.Where( "user_id = ? AND type = ?", u.ID, step ).Select("extra").First ( &prov )
            newSeed := auth.SeedStep ( step, prov.Extra );
            if ChangeLoginToken ( ctx, u, step, newSeed ) == true {
                err = errors.New ("Invalid Auth Operation")
            }
        }
        return ctx.Redirect ( http.StatusFound, "/login?error="+err.Error() )
    }

    return ctx.Redirect (http.StatusFound, "/login" );
}

// redirects to the login form
func loginHandler ( ctx echo.Context ) error {
    return ctx.Redirect ( http.StatusFound, "/login" )
}

// Renders first step login form
// /login
func loginHandler_ ( ctx echo.Context ) error {
    if s.cfg.Install == true {
        return ctx.Redirect(http.StatusFound, "/install/1")
    }
    u := core.User{};
    if UserFromSession( &u, ctx ) == true {
       return dashboardHandler ( ctx, u );
    }

    step, stepExtra := loginStepFromToken ( ctx );
    view := auth.LoginView ( step )
    if step == "fail" || view == "" {
        RemoveLoginToken ( ctx );
        return ctx.Redirect ( http.StatusFound, "/login" );
    } else if step == "fin" {
        // this should not occur, it should get redirected to the dashboard handler
        HTTPError ( 500, ctx );
    }

    return ctx.Render (http.StatusOK, view, echo.Map{
        "navPage": "login",
        "authExtra": stepExtra,
    });
}

// /impress
func impressHandler ( ctx echo.Context ) error {
    u := core.User{};
    if UserFromSession ( &u, ctx ) == true {
        return ctx.Render (http.StatusOK, "impress", echo.Map{
            "user": u,
            "navPage": "",
        })
    }
    return ctx.Render (http.StatusOK, "impress", echo.Map{
        "navPage": "impress",
    })
}

// /integration
func integrationHandler ( ctx echo.Context ) error {
    u := core.User{};
    if UserFromSession ( &u, ctx ) != true {
        return ctx.Redirect ( http.StatusFound, "/" )
    }
    var webhookURL string = ctx.Scheme() + "://" + ctx.Request().Host + "/push/hook"
    return ctx.Render (http.StatusOK, "integration", echo.Map{
        "user": u,
        "navPage": "integration",
        "webhookURL": webhookURL,
    })
}
