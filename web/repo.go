// repository views and handlers

package web

import (
    "fmt"
    "strings"
    "strconv"
    //"regexp"
    "net/http"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/build"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

func aclString ( id int ) string {
    return core.AclToString(id)
}

// type of repo and its state for the view render functions
type repo_state struct {
    R core.Repository       // repo
    State string            // its last build state
}

func accessibleRepoIds ( user core.User ) []uint {
    var r []uint
    s.db.Table("acls").Select("repository_id").Where("access_level > 0 AND (user_id = ? OR group_id IN (?))", user.ID,
        s.db.Table("user_groups").Select("group_id").Where("user_id = ?", user.ID)).Find( &r );
    return r;
}

// /repo
// list of repositories
func repoHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var r []core.Repository;
    var mine string = ctx.QueryParam ( "mine" )
    if mine == "" {
        if ( user.Superuser ) {
            s.db.Order("name asc").Find ( &r );
        } else {
            ids := accessibleRepoIds( user )
            s.db.Where("user_id = ? OR id IN (?)", user.ID, ids).Order("name asc").Find ( &r )
        }
    } else {
        s.db.Where("user_id = ?", user.ID).Order("name asc").Find ( &r );
    }

    var rx []repo_state;
    for _,v := range r {
        var bx core.Build
        s.db.Where ( "repository_id = ?", v.ID).Order("started_at DESC").Limit(1).First( &bx );
        rx = append ( rx, repo_state { R: v, State: strings.ToLower ( bx.Status ) } )
    }

    return ctx.Render ( http.StatusOK, "repo", echo.Map{
        "navPage": "repo",
        "user": user,
        "repositories": rx,
        "u": nil,
    })
}

// /repo/state/:id
// return a corresponding image for README files of projects
// only when the repo is public
const STATE_PASSED string = "static/img/build_passed.png"
const STATE_MISSED string = "static/img/build_miss.png"
const STATE_FAILED string = "static/img/build_failed.png"
func repoStateHandler ( ctx echo.Context ) error {
    id, err := strconv.Atoi(ctx.Param ("id"));
    if err == nil {
        var r core.Repository;
        s.db.Select ( "id,public" ).Where ("id = ?", id).First ( &r );
        if r.Public == true {
            var b core.Build;
            s.db.Select ( "id, status" ).Where("repository_id = ? AND status IN ('finished', 'failed')", r.ID).Order("finished_at DESC").Limit(1).First(&b);
            if b.ID > 0 {
                if b.Status == "finished" {
                    return ctx.File ( STATE_PASSED );
                } else if b.Status == "failed" {
                    return ctx.File ( STATE_FAILED );
                }
            }
        }
    }
    return ctx.File ( STATE_MISSED );
}

// /repo/edit/:id
func repoEditHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    id, _ := strconv.Atoi(ctx.Param("id"))
    var r core.Repository;
    var bs []core.BuildScript;
    s.db.Where("id = ?", strconv.Itoa(id)).First ( &r )
    s.db.Where("repository_id = ?", r.ID).Find( &bs );
    if r.ID <= 0 {
        return HTTPError ( 404, ctx );
    }


    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclEdit ) {
        return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(id) )
    }

    var pbs, rbs string
    for _,v := range bs {
        if v.EventType == "push" {
            pbs = v.ShellScript;
        }
        if v.EventType == "release" {
            rbs = v.ShellScript;
        }
    }

    return ctx.Render ( http.StatusOK, "repo_edit", echo.Map{
        "mode": "edit",
        "navPage": "repo",
        "user": user,
        "repo": r,
        "errors": []string{},
        "success": []string{},
        "PushBuildScript": pbs,
        "ReleaseBuildScript": rbs,
    })
}

// /repo/build/:id
// start a simple build for the repo
func repoBuildHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    var r core.Repository
    id, _ := strconv.Atoi(ctx.Param("id"))
    s.db.Where("id = ?", id).First ( &r )
    if r.ID > 0 {
        acl := core.AclCheck ( s.db, user, r )
        if ( acl < core.AclExecute ) {
            return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(id)+"?error=Permission Error" )
        }

        b := build.BuildNow ( r, s.db )
        return ctx.Redirect ( http.StatusFound, "/build/"+strconv.Itoa(int(b)) )
    }
    return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(id) )
}

// /repo/add
func repoAddHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    return ctx.Render ( http.StatusOK, "repo_edit", echo.Map{
        "mode": "add",
        "navPage": "repo",
        "user": user,
        "repo": core.Repository{},
        "errors": []string{},
        "success": []string{},
    })
}

func repoMembersHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, _ := strconv.Atoi(ctx.Param("id"))
    var r core.Repository;
    s.db.Where("id = ?", strconv.Itoa(id)).First ( &r )
    if r.ID <= 0 {
        return HTTPError ( 404, ctx );
    }

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclView ) {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Permission Error" )
    }

    var superusers []core.User
    s.db.Select("name, id, superuser").Where("superuser = true OR id = ?", r.UserID).Order("name ASC").Find ( &superusers )

    var myGroups []core.Group
    s.db.Where("id IN (?)", s.db.Table("user_groups").Select("group_id").Where("user_id = ?", user.ID)).Order("name ASC").Find(&myGroups)

    // TODO ugly
    var users []core.User
    s.db.Select("id", "name").Find ( &users )

    var aclGroup []core.Acl
    s.db.Joins("Group").Where("repository_id = ? AND group_id > 0", r.ID ).Order("name ASC").Find ( &aclGroup )
    var aclUser []core.Acl
    s.db.Joins("User").Where("repository_id = ? AND user_id > 0", r.ID ).Order("name ASC").Find ( &aclUser )

    return ctx.Render ( http.StatusOK, "repo_members_edit", echo.Map{
        "navPage": "repo",
        "user": user,
        "users": users,
        "repo": r,
        "acl": acl,
        "aclGroup": aclGroup,
        "aclUser": aclUser,
        "aclLevel": core.AclStrings,
        "superusers": superusers,
        "myGroups": myGroups,
        "errors": []string{},
        "success": []string{},
    })
}

func repoMembersPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, _ := strconv.Atoi(ctx.Param("id"))
    var r core.Repository;
    s.db.Where("id = ?", id).First ( &r )
    if r.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Invalid repo" )
    }

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclComplete ) {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Permission Error" )
    }

    aclLevel, err := strconv.Atoi ( ctx.FormValue ("aclLevel") );
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Invalid privilege level" )
    }
    if aclLevel < int(core.AclForbidden) && aclLevel > int(core.AclComplete) {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Invalid privileeg level" )
    }

    usr_id := ctx.FormValue ( "aclUserId" )
    grp_id := ctx.FormValue ( "aclGroupId" )
    acl_id := ctx.FormValue ( "aclId" )
    fmt.Printf("%+v", ctx.Request().Form )
    fmt.Println ( usr_id, grp_id, acl_id )
    if grp_id == "" && acl_id == "" && usr_id == "" {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Group not found" )
    }

    var newacl core.Acl
    if acl_id != "" {
        aclid, err := strconv.Atoi ( acl_id )
        if err != nil {
            return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Membership not found" )
        }

        s.db.Model( &core.Acl{} ).Where("id=? AND repository_id = ?", aclid, r.ID).Update("access_level",aclLevel)
    } else {
        if grp_id != "" {
            // groups
            group_id, err := strconv.Atoi ( grp_id )
            if err != nil {
                return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Group not found" )
            }

            var group core.Group
            s.db.Where("id = ?", group_id).First ( &group )
            if group.ID <= 0 {
                return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Group not found" )
            }

            s.db.Where("group_id=? AND repository_id = ?", group_id, r.ID).First( &newacl )
            if newacl.ID > 0 {
                s.db.Model( &core.Acl{} ).Where ( "id = ?", newacl.ID ).Update("access_level", aclLevel)
            } else {
                newacl.AccessLevel = core.AclLevel(aclLevel)
                newacl.RepositoryID = r.ID
                newacl.Group = &group
                s.db.Save(&newacl)
            }
        } else {
            // user
            user_id, err := strconv.Atoi ( usr_id )
            if err != nil {
                return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=User not found" )
            }

            var user core.User
            s.db.Where("id = ?", user_id).First ( &user )
            if user.ID <= 0 {
                return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=User not found" )
            }

            s.db.Where("user_id=? AND repository_id = ?", user_id, r.ID).First( &newacl )
            if newacl.ID > 0 {
                s.db.Model( &core.Acl{} ).Where ( "id = ?", newacl.ID ).Update("access_level", aclLevel)
            } else {
                newacl.AccessLevel = core.AclLevel(aclLevel)
                newacl.RepositoryID = r.ID
                newacl.User = &user
                s.db.Save(&newacl)
            }
        }
    }

    return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?success=Membership successfully added" )
}

func repoMembersDeleteHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, _ := strconv.Atoi(ctx.Param("id"))
    var r core.Repository;
    s.db.Where("id = ?", id).First ( &r )
    if r.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Invalid repo" )
    }

    acl := core.AclCheck ( s.db, user, r )
    if ( acl < core.AclComplete ) {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Permission Error" )
    }

    acl_id, err := strconv.Atoi(ctx.Param("acl_id"))
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Invalid ACL" )
    }

    var acls core.Acl
    acls.ID = uint(acl_id)
    result := s.db.Where("repository_id = ?", r.ID).Delete(&acls)
    if result.RowsAffected == 0 {
        return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?error=Invalid ACL" )
    }

    return ctx.Redirect ( http.StatusFound, "/repo/members/"+strconv.Itoa(id)+"?success=Deleted ACL" )
}

// /repo/:id or /repo/view/:id
// shows the list of builds for a repo
func repoViewHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, _ := strconv.Atoi(ctx.Param("id"))
    pg, _ := strconv.Atoi(ctx.Param("page"))
    var page int64 = int64(pg)
    if page <= 0 {
        page = 1;
    }
    offset := (page-1)*10;
    var r core.Repository;
    var u core.User;
    s.db.Where("id = ?", id).First ( &r )
    if r.ID <= 0 {
        return HTTPError ( 404, ctx );
    }
    // auth check
    acl := core.AclCheck ( s.db, user, r );
    if acl < core.AclView {
        ctx.Redirect ( http.StatusFound, "/repo?error=Can't view that repository" )
    }

    s.db.Select("name, id").Where("id = ?", r.UserID).First ( &u )
    var cnt int64;
    s.db.Model( core.Build{} ).Where("repository_id = ?", r.ID).Count ( &cnt )
    var pageMax int64 = (cnt / 10) + 1
    if offset >= cnt {
        offset = (cnt / 10) * 10;
        page = (offset / 10) + 1
    }

    cnt = 0;
    var b []core.Build;
    var bx core.Build;
    s.db.Where("repository_id = ?", r.ID).Order("started_at DESC").Offset( int((page-1)*10) ).Limit(10).Find ( &b )
    s.db.Select ("id").Where("repository_id = ? AND status NOT IN ('finished', 'failed')", r.ID ).First ( &bx )

    // TODO pagination surely can be done simpler
    var pageRange []int64
    if pageMax > 9 {
        pageRange = make([]int64, 11)
        pageRange[0] = 1;
        pageRange[1] = 2;
        pageRange[2] = 3;
        if page == 4 || page == 5 {
            pageRange[3] = 4;
            pageRange[4] = 5;
            pageRange[5] = 6;
            pageRange[6] = 7;
            pageRange[7] = -1;  // => ...
        } else if page == pageMax - 4 || page == pageMax - 3 {
            pageRange[3] = -1;
            pageRange[4] = pageMax - 6;
            pageRange[5] = pageMax - 5;
            pageRange[6] = pageMax - 4;
            pageRange[7] = pageMax - 3;
        } else {
            pageRange[3] = -1;
            if page < pageMax - 4 && page > 5 {
                pageRange[4] = page - 1
                pageRange[5] = page
                pageRange[6] = page + 1
            } else {
                pageRange[4] = pageMax/2
                pageRange[5] = pageMax/2 + 1
                pageRange[6] = pageMax/2 + 2
            }
            pageRange[7] = -1
        }
        pageRange[8] = pageMax - 2;
        pageRange[9] = pageMax - 1;
        pageRange[10] = pageMax
    } else {
        pageRange = make([]int64, pageMax)
        var i int64 = 1;
        for i <= pageMax {
            pageRange[i-1] = i;
            i++;
        }
    }

    CalcBuildDuration ( b )
    var stateURL string = ctx.Scheme() + "://" + ctx.Request().Host + "/repo/state/" + strconv.FormatUint(uint64(r.ID),10)
    return ctx.Render ( http.StatusOK, "repo_view", echo.Map{
        "navPage": "repo",
        "acl": acl,
        "user": user,
        "u": u,
        "repo": r,
        "stateUrl": stateURL,
        "builds": b,
        "unfinishedBuild": bx.ID,
        "page" : page,
        "pageMax" : pageMax,
        "pageRange": pageRange,
    })
}

// Check user inputs from adding or editing repository information views.
// Then commit them to the database.
func repoPostChecker ( ctx echo.Context, user core.User, edit bool ) ([]string, []string, uint) {
    var name string;
    var cloneUrl string;
    var defaultBranch string;
    var pushBuildScript string;
    var releaseBuildScript string
    lb := false
    wh := false
    keepBuilds := false;
    public := false;

    var id int;
    var err error;
    id = -1;
    if edit == true {
        id, err = strconv.Atoi(ctx.Param ("id"))
        if err != nil {
            return []string{"Invalid ID"},[]string{},0
        }
    }
    name = ctx.FormValue ("repo_name")
    cloneUrl = ctx.FormValue ("repo_clone_url")
    defaultBranch = ctx.FormValue ("repo_default_branch")
    pushBuildScript = ctx.FormValue ("repo_push_build_script")
    releaseBuildScript = ctx.FormValue ( "repo_release_build_script" )
    if ctx.FormValue ("repo_public_enable") == "on" {
        public = true
    }
    if ctx.FormValue ("repo_keepbuilds_enable") == "on" {
        keepBuilds = true
    }
    if ctx.FormValue("repo_webhook_enable") == "on" {
        wh = true
    }
    if ctx.FormValue ("repo_localbuild_enable") == "on" {
        lb = true
    }

    if len(defaultBranch) <= 0 {
        defaultBranch = "master"
    }

    if len(name) <= 0 || len(cloneUrl) <= 0 {
        return []string{"Did you input anything?"},[]string{},0
    }

    /*var cnt int64;
    s.db.Model( &core.Repository{} ).Where("name = ?", name).Count ( &cnt );
    if cnt >= 1 {
        return []string{"A repository with the same name already exists"}, []string{}, 0
    }*/

    // TODO check if repo exists and if I can read from it!

    var secret string;
    var r core.Repository
    if edit == false {
        var x int64 = 1
        for x > 0 {
            secret = authSecretGenerator()
            s.db.Model ( &core.Repository{} ).Where ("secret = ?", secret ).Count ( &x )
        }
        bp := core.BuildScript { EventType: "push", ShellScript: pushBuildScript };
        br := core.BuildScript { EventType: "release", ShellScript: releaseBuildScript };

        s.db.Save ( &bp );
        s.db.Save ( &br );
        r = core.Repository{ Name: name, CloneUrl: cloneUrl,
                             DefaultBranch : defaultBranch, Secret: secret,
                             BuildCount: 0, BuildScripts: []core.BuildScript{ bp, br },
                             WebHookEnable: wh, LocalBuildEnable: lb,
                             KeepBuilds: keepBuilds, Public: public,
                             UserID: user.ID}

        s.db.Create ( &r )
        if r.ID <= 0 {
            return []string{"Could not create the repository"},[]string{},0
        }
    } else {
        secret = ctx.FormValue ( "repo_secret")
        s.db.Where ("id = ?", id).First ( &r )
        // auth check
        acl := core.AclCheck ( s.db, user, r );
        if acl < core.AclEdit {
            ctx.Redirect ( http.StatusFound, "/repo?error=Can't view that repository" )
        }

        br := core.BuildScript{};
        bp := core.BuildScript{};

        s.db.Where("repository_id = ? AND event_type = ?", r.ID, "push").First(&bp);
        s.db.Where("repository_id = ? AND event_type = ?", r.ID, "release").First(&br);

        if bp.ID <= 0 {
            bp.EventType = "push";
            r.BuildScripts = append ( r.BuildScripts, bp );
        }

        if br.ID <= 0 {
            br.EventType = "release";
            r.BuildScripts = append ( r.BuildScripts, br );
        }

        if r.ID <= 0 {
            return []string{"Invalid ID"},[]string{},0
        }

        r.WebHookEnable = wh
        r.LocalBuildEnable = lb
        r.Name = name
        r.CloneUrl = cloneUrl
        r.DefaultBranch = defaultBranch
        r.Secret = secret
        //r.BuildScript = buildScript
        r.KeepBuilds = keepBuilds
        r.Public = public

        br.ShellScript = releaseBuildScript
        bp.ShellScript = pushBuildScript

        err = s.db.Save ( &r ).Error
        if err != nil {
            return []string{err.Error()}, []string{}, r.ID
        }
        err = s.db.Save ( &bp ).Error
        if err != nil {
            return []string{err.Error()}, []string{}, r.ID
        }
        err = s.db.Save ( &br ).Error
        if err != nil {
            return []string{err.Error()}, []string{}, r.ID
        }
    }
    return []string{}, []string{"ok"}, r.ID
}

// POST /repo/edit/:id
func repoEditPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    err, _, rid  := repoPostChecker ( ctx, user, true );
    id, err2 := strconv.Atoi( ctx.Param ("id") )
    if len(err) > 0 {
        // the edit operation did not work, no trustworthy id in rid
        if err2 == nil {
            var r core.Repository
            s.db.Select("id").Where ("id = ?", strconv.Itoa(id)).First (&r);
            if r.ID > 0 {
                return ctx.Redirect ( http.StatusFound, "/repo/edit/" + strconv.Itoa(id) )
            }
            return ctx.Render ( http.StatusOK, "repo_edit", echo.Map{
                "mode": "add",
                "navPage": "repo",
                "user": user,
                "u": core.Repository{},
                "errors": err,
                "success": []string{},
            })
        }
    }
    return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.FormatUint( uint64(rid), 10 ) )
}

// POST /repo/add
func repoAddPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    err, _, rid := repoPostChecker ( ctx, user, false );
    if len(err) > 0 {
        return ctx.Render ( http.StatusOK, "repo_edit", echo.Map{
            "mode": "add",
            "navPage": "repo",
            "user": user,
            "u": core.Repository{},
            "errors": err,
            "success": []string{},
        })
    }
    return ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.FormatUint(uint64(rid), 10) )
}

// /repo/delete/:id
// delete a repository, its corresponding builds and artifacts
func repoDeleteHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi ( ctx.Param ("id") )
    if err != nil {
        // todo: error propagation
        return ctx.Redirect ( http.StatusFound, "/repo?error=Repository not found");
    }

    var r core.Repository;
    var builds []core.Build;
    s.db.Where("id = ?", id).First(&r);
    if r.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/repo?error=Repository not found");
    }

    // auth check
    acl := core.AclCheck ( s.db, user, r );
    if acl < core.AclDelete {
        ctx.Redirect ( http.StatusFound, "/repo/view/"+strconv.Itoa(id)+"?error=Permission Error" )
    }

    // delete all builds
    s.db.Select("id").Where("repository_id = ?", id).Find( &builds )
    for _, b := range builds {
        buildDelete ( b.ID )
    }
    s.db.Delete(&r);

    // delete all build-scripts
    s.db.Select("id").Where("repository_id = ?", id).Delete ( &core.BuildScript{} )

    return ctx.Redirect ( http.StatusFound, "/repo");
}
