// user views and handlers

package web

import (
    "net/url"
    "strings"
    "strconv"
    "regexp"
    "net/http"
    "html/template"
    "codeberg.org/snaums/mvoCI/core"
    "codeberg.org/snaums/mvoCI/auth"

    //"golang.org/x/crypto/bcrypt"
    //"github.com/jinzhu/gorm"
    "github.com/labstack/echo/v4"
    //"github.com/labstack/echo/middleware"
)

// /user
// list of users
func userHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    var u []core.User;
    s.db.Order("name asc").Find ( &u );

    return ctx.Render ( http.StatusOK, "user", echo.Map{
        "navPage": "user",
        "user": user,
        "users": u,
    })
}

// POST /user/:id/token/add
// add a API token to the current user with a given name
func userTokenAdd ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    if user.ID != uint(id) && user.Superuser != true {
        return HTTPError ( 401, ctx );
    }

    name := ctx.FormValue("user_token_name");
    var l core.LoginToken;
    if NewAPIToken ( &l, ctx, user, name ) {
        return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(user.ID)) )
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(user.ID)) )
}

func userGroupHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    group_id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil || group_id <= 0 {
        return ctx.Redirect ( http.StatusFound, "/user?error=User not found")
    }

    var g core.Group
    s.db.Where("id = ?", group_id).First ( &g )
    if g.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/user?error=Group not found")
    }

    var u []core.User
    s.db.Where("id IN (?)", s.db.Table("user_groups").Select("user_id").Where("group_id = ?", group_id) ).Select("id", "name", "superuser").Find ( &u )

    var amIin bool = false
    if !user.Superuser {
        for _, ux := range u {
            if ux.ID == user.ID {
                amIin = true
                break
            }
        }
    }

    if user.Superuser || amIin {
        return ctx.Render ( http.StatusOK, "user_group", echo.Map {
            "navPage": "user",
            "group": g,
            "users": u,
            "user": user,
        })
    } else {
        return ctx.Redirect ( http.StatusFound, "/user?error=Permission Error")
    }
}

func userLeaveGroupHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    user_id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil || user_id <= 0 {
        return ctx.Redirect ( http.StatusFound, "/user?error=User not found")
    }
    if user.ID != uint(user_id) && user.Superuser != true {
        return ctx.Redirect ( http.StatusFound, "/user?error=Permission Error")
    }

    var u core.User
    var g core.Group
    s.db.Where("id = ?", user_id).First ( &u )
    if u.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/user?error=User not found")
    }

    group_id, err := strconv.Atoi ( ctx.Param ( "group_id" ))
    if err != nil || group_id <= 0 {
        return ctx.Redirect ( http.StatusFound, "/user/"+ strconv.Itoa(user_id) + "?error=GroupEmail not found")
    }

    s.db.Where("id = ?", group_id).First ( &g )
    if g.ID <= 0 {
        return ctx.Redirect ( http.StatusFound, "/user/"+ strconv.Itoa(user_id) + "?error=Group not found")
    }

    s.db.Table("user_groups").Delete(nil, "group_id = ? AND user_id = ?", group_id, user_id)

    var cnt int64
    s.db.Table("user_groups").Where("group_id = ?", group_id ).Count( &cnt )
    if cnt == 0 {
        s.db.Delete(core.Acl{}, "group_id = ?", group_id )
        s.db.Delete(core.Group{}, "id = ?", group_id )
        return ctx.Redirect ( http.StatusFound, "/user/"+ strconv.Itoa(user_id) + "?success=Left the group as last member; deleted the group")
    }

    return ctx.Redirect ( http.StatusFound, "/user/"+ strconv.Itoa(user_id) + "?success=Left the group")
}

func userAddGroupPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil || id <= 0 {
        return HTTPError ( 500, ctx );
    }
    if user.ID != uint(id) && user.Superuser != true {
        return HTTPError ( 401, ctx );
    }

    var u core.User
    s.db.Where("id=?", id).First( &u )
    if u.ID != uint(id) {
        return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(id))+"?error=User not found")
    }

    name := ctx.FormValue ("user_group_name")
    if name == "Everyone" || name == "" {
        return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(u.ID))+"?error=Invalid Group Name")
    }

    var grp core.Group
    var users []uint
    s.db.Where("name = ?", name).First( &grp )
    if grp.ID > 0 {
        s.db.Table("user_groups").Select("user_id").Where("group_id=?", grp.ID).Find(&users);
        var amIMember bool = false
        for _, v := range users {
            if v == u.ID {
                return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(u.ID))+"?error=This user is already a member of that group")
            }
            if v == user.ID {
                amIMember = true
            }
        }

        // I can only add members to groups I am a member of
        if user.Superuser != true && amIMember == false {
            return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(u.ID))+"?error=The name is already taken")
        }

        grp.User = append (grp.User, u)
    } else {
        grp = core.Group{}
        grp.Name = name
        grp.User = []core.User{ u }
    }

    s.db.Save( &grp )
    if grp.ID > 0 {
        return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(u.ID))+"?succes=Created group "+name)
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(u.ID))+"?error=Cannot store new group")
}

// /user/:id/token/invalidate/:tid
// delete the API token from user id, with the token id tid
func userTokenInvalidate ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi ( ctx.Param("id") )
    if err != nil {
        return HTTPError ( 500, ctx );
    }
    if user.ID != uint(id) && user.Superuser != true {
        return HTTPError ( 401, ctx );
    }

    tid, err := strconv.Atoi ( ctx.Param("tid") )
    if err != nil {
        return HTTPError ( 500, ctx );
    }

    s.db.Where ("id = ? AND type != 'login' AND user_id = ?", tid, user.ID ).Delete( &core.LoginToken{} );
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.Itoa(int(user.ID)) )
}

// /user/repo/:id
// show repositories owned by a given user
func userRepoHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    id, err := strconv.Atoi( ctx.Param("id") )
    if err != nil {
        return HTTPError(401, ctx)
    }

    var u core.User;
    s.db.Where("id = ?", id).First ( &u )
    if u.ID > 0 && (user.ID == u.ID || user.Superuser) {
        var r []core.Repository;
        s.db.Where("user_id = ?", u.ID).Order("name ASC").Find ( &r )

        // repo_state type defined in web/repo.go
        var rx []repo_state;
        for _,v := range r {
            var bx core.Build
            s.db.Where ( "repository_id = ?", v.ID).Order("started_at DESC").Limit(1).First( &bx );
            rx = append ( rx, repo_state { R: v, State: strings.ToLower ( bx.Status ) } )
        }

        return ctx.Render ( http.StatusOK, "repo", echo.Map {
            "navPage": "repo",
            "user": user,
            "repositories": rx,
            "u": u,
        })
    }
    return ctx.Redirect ( http.StatusFound, "/user/" + strconv.Itoa(id) )
}

// /user/edit/:id
func userEditHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    var u core.User;
    var idstring string = ctx.Param("id")
    if idstring == "me" {
        u = user
    } else {
        id, err := strconv.Atoi(ctx.Param("id"))
        if err != nil {
            return ctx.Redirect ( http.StatusFound, "/user/me?error=Invalid ID" )
        }
        s.db.Where("id = ?", strconv.Itoa(id)).First ( &u );
        if u.ID <= 0 {
            return HTTPError ( 404, ctx );
        }
    }

    navPage := "user"
    if (u.ID == user.ID) {
        navPage = "profile";
    } else {
        if user.Superuser != true {
            return HTTPError ( 401, ctx );
        }
    }

    // list of user tokens
    var token []core.LoginToken
    s.db.Where ( "user_id = ?", u.ID).Find ( &token );

    var groups []core.Group
    s.db.Where("id IN (?)", s.db.Table("user_groups").Select("group_id").Where("user_id=?", u.ID)).Order("name ASC").Find( &groups );

    // get listing of auth modules and their config for the user
    authSetting := auth.ListAuth ( u )
    return ctx.Render ( http.StatusOK, "user_edit", echo.Map{
        "m": "edit",
        "navPage": navPage,
        "user": user,
        "u": u,
        "groups": groups,
        "Auth": authSetting,
        "MainProvider": auth.HumanName ( u.AuthProvider ),
        "errors": []string{},
        "success": []string{},
        "LoginToken": token,
    })
}

// /user/me/auth/enable/:module
// enable a given auth module, may just do, or may go to a setup-page
func userAuthModuleEnable ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    str, err := auth.AuthEnable ( s.db, &user, module )
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me?error="+err.Error())
    }

    if str == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" enabled")
    } else {    // if str == stage2 it is transferred to the 
        // second step enabling is necessary
        return ctx.Redirect ( http.StatusFound, "/user/me/auth/setup/"+module+"?challenge="+url.QueryEscape(str) )
    }
    return ctx.Redirect ( http.StatusFound, "/user/me" )
}

// /user/me/auth/setup/:module
// show a setup view, present secrets and a challenge to the user
func userAuthModuleSetupView ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    challenge := ctx.QueryParam ( "challenge" )
    view, challenge := auth.SetupView(s.db, user, module, challenge)
    if view == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    return ctx.Render ( http.StatusOK, view, echo.Map{
        "navPage": "profile",
        "user": user,
        "module": module,
        "challenge": template.HTML ( challenge ),
        "errors": []string{ctx.QueryParam("error")},
        "success": []string{},
    })
}

// POST /user/me/auth/setup/:module
// take the user input and try to commit enabling the module
func userAuthModuleCommit ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=" + url.QueryEscape("Module not found"))
    }

    userInput := ctx.FormValue("userinput")
    if userInput == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me/"+module+"?error=Module not found!!")
    }

    err := auth.AuthEnableCommit ( s.db, &user, module, userInput )
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me/auth/setup/"+module+"?challenge="+url.QueryEscape(err.Error()) )
    }

    return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" enabled")
}

// /user/me/auth/config/:module
// configure an auth module
func userAuthModuleConfigView ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    view, info := auth.ConfigView( s.db, user, module )
    if view == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    return ctx.Render ( http.StatusOK, view, echo.Map{
        "navPage": "profile",
        "user": user,
        "module": module,
        "info": info,
        "errors": []string{ctx.QueryParam("error")},
        "success": []string{},
    })

}

// POST /user/me/auth/config/:module
// take user input and apply the config change
func userAuthModuleConfigCommit ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    params, _ := ctx.FormParams()
    err := auth.ConfigCommit(s.db, user, module, params)
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me?error="+err.Error())
    }

    return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" configured successfully")
}

// /user/me/auth/disable/:module
// disable an auth step
func userAuthModuleDisable ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }

    module := ctx.Param ( "module" )
    if module == "" {
        return ctx.Redirect ( http.StatusFound, "/user/me?error=Module not found")
    }

    err := auth.AuthDisable ( s.db, &user, module )
    if err != nil {
        return ctx.Redirect ( http.StatusFound, "/user/me?error="+err.Error())
    }
    return ctx.Redirect ( http.StatusFound, "/user/me?success="+module+" disabled")
}

// /user/delete/:id
func userDeleteHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }
    id, _ := strconv.Atoi(ctx.Param("id"))
    if id == int(user.ID) {
        ctx.Render ( http.StatusOK, "user_edit", echo.Map{
            "m": "edit",
            "navPage": "profile",
            "user": user,
            "u": user,
            "errors": []string{"You cannot delete yourself"},
            "success": []string{},
        })
    }
    var u core.User;
    s.db.Where("id = ?", id).First(&u);
    s.db.Delete(&u);
    return ctx.Redirect ( http.StatusFound, "/user");
}

// /user/add
// create a new user
func userAddHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }
    return ctx.Render ( http.StatusOK, "user_edit", echo.Map{
        "m": "add",
        "navPage": "user",
        "user": user,
        "u": core.User{},
        "errors": []string{},
        "success": []string{},
    })
}

func SetupAuthProvider ( user *core.User ) {
    user.AuthProvider = "native"
    user.AuthExtra = "{\"Enable\":false, \"Order\":\"\"}"
}

// check inputs on add or edit views and commit the changed data to database
func userPostChecker ( ctx echo.Context, edit bool ) ([]string, []string, uint) {
    var id int;
    var u core.User;
    if edit == true {
        id, _ = strconv.Atoi ( ctx.Param("id") );
        s.db.Where("ID = ?", id).First (&u)
    }
    var name string;
    var pass1 string;
    var pass2 string;
    var email string;
    var superuser string;

    name = ctx.FormValue ( "user_name" );
    email = ctx.FormValue ( "user_email" );
    pass1 = ctx.FormValue ( "user_password" );
    pass2 = ctx.FormValue ( "user_password2" );
    superuser = ctx.FormValue ("user_superuser" );

    if pass1 != pass2 {
       return []string{"Passwords were not identical"},[]string{},0
    }
    if len(email) <= 0 || ((len(name) <= 0|| len(pass1) <= 0) && edit == false)  {
       return []string{"Did you input anything?"},[]string{},0
    }
    var cnt int64 = 0
    s.db.Model(&core.User{}).Where("name = ?", name).Count(&cnt)
    if cnt > 0 {
       return []string{"Username already taken"},[]string{},0
    }
    re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
    if re.MatchString ( email ) == false {
        return []string{"You have to input an email-address, you know?"},[]string{},0
    }

    if edit == false {
        u = core.User{};
        u.Name = name;
    }
    u.Email = email;
    if superuser == "on" {
        u.Superuser = true;
    } else {
        u.Superuser = false;
    }
    if len(pass1) > 0 {
        if ( auth.AuthSetSecret ( &u, "native", pass1, pass2 ) != nil ) {
            return []string{"Passwords were not identical"},[]string{},0
        }
    }

    SetupAuthProvider ( &u )

    if edit == false {
        s.db.Create( &u );
        var group core.Group
        s.db.Select("id").Where("name=?", "Everyone").First(&group)
        s.db.Model ( &group ).Association("User").Append([]*core.User{&u} )
    } else {
        s.db.Model(&core.User{}).Where("id = ?", u.ID).Save ( &u );
    }
    return []string{}, []string{"ok"},u.ID
}

// POST /user/edit/:id
func userEditPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }

    id, _ := strconv.Atoi( ctx.Param("id") );
    err, _, uid := userPostChecker ( ctx, true );
    if len(err) >= 0 {
        var u core.User;
        s.db.Where("id = ?", id).First(&u);
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.FormatUint(uint64(uid), 10, ) )
}

// POST /user/add
func userAddPostHandler ( ctx echo.Context ) error {
    user := core.User{};
    if UserFromSession ( &user, ctx ) != true {
       return loginHandler ( ctx );
    }
    if user.Superuser != true {
        HTTPError ( 401, ctx );
    }

    err, _, uid := userPostChecker ( ctx, false );
    if len(err) > 0 {
        return ctx.Render ( http.StatusOK, "user_edit", echo.Map{
            "m": "add",
            "navPage": "user",
            "user": user,
            "u": core.User{},
            "errors": err,
            "success": []string{},
        })
    }
    return ctx.Redirect ( http.StatusFound, "/user/edit/"+strconv.FormatUint(uint64(uid), 10) )
}
